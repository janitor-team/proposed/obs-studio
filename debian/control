Source: obs-studio
Section: video
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Carl Fürstenberg <azatoth@gmail.com>,
 Sebastian Ramacher <sramacher@debian.org>
Build-Depends:
 cmake,
 debhelper-compat (= 13),
 dh-sequence-python3,
 dh-sequence-scour,
 gir1.2-rsvg-2.0,
 libasound2-dev,
 libavcodec-dev,
 libavdevice-dev,
 libavfilter-dev,
 libavformat-dev,
 libavutil-dev,
 libcurl4-gnutls-dev | libcurl-dev,
 libdbus-1-dev,
 libfontconfig-dev,
 libfreetype6-dev,
 libgl1-mesa-dev | libgl-dev,
 libjack-jackd2-dev,
 libjansson-dev (>= 2.5),
 libluajit-5.1-dev,
 libmbedtls-dev,
 libpipewire-0.3-dev,
 libpulse-dev,
 libqt5svg5-dev,
 libqt5x11extras5-dev,
 libsimde-dev,
 libspeexdsp-dev,
 libswresample-dev,
 libswscale-dev,
 libudev-dev,
 libv4l-dev,
 libvlc-dev,
 libwayland-dev,
 libx11-dev,
 libx11-xcb-dev,
 libx264-dev,
 libxcb-randr0-dev,
 libxcb-shm0-dev,
 libxcb-xfixes0-dev,
 libxcb-xinerama0-dev,
 libxcb-xinput-dev,
 libxcomposite-dev,
 libxinerama-dev,
 python3-dev,
 python3-docutils (>= 0.6),
 python3-gi-cairo,
 qtbase5-dev,
 qtbase5-private-dev,
 swig
Standards-Version: 4.5.1
Homepage: https://obsproject.com
Vcs-Browser: https://salsa.debian.org/multimedia-team/obs-studio
Vcs-Git: https://salsa.debian.org/multimedia-team/obs-studio.git
Rules-Requires-Root: no

Package: obs-studio
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, ${python3:Depends}
Recommends: obs-plugins (= ${binary:Version})
Suggests:
 v4l2loopback-dkms,
 policykit-1
Description: recorder and streamer for live video content
 OBS Studio is designed for efficiently recording and streaming live video
 content. It supports live RTP streaming to various streaming sites.
 .
 Other features include:
  * Encoding using H264 (x264) and AAC
  * Unlimited number of scenes and sources
  * File output to MP4 or FLV
  * GPU-based game capture for high performance game streaming
  * Bilinear or lanczos3 resampling
 .
 Note that OBS Studio requires an OpenGL 3.2 compatible video card.

Package: obs-plugins
Architecture: any
Built-Using: ${simde:Built-Using}
Depends: ${misc:Depends}, ${shlibs:Depends}
Recommends: vlc
Description: recorder and streamer for live video content (plugins)
 OBS is designed for efficiently recording and streaming live video content. It
 supports live RTP streaming to various streaming sites.
 .
 This package contains some extra plugins (ALSA, jack, decklink, vlc).

Package: libobs0
Multi-Arch: same
Section: libs
Architecture: any
Built-Using: ${simde:Built-Using}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: recorder and streamer for live video content (shared library)
 OBS Studio is designed for efficiently recording and streaming live video
 content. It supports live RTP streaming to various streaming sites.
 .
 This package contains the shared library libobs.

Package: libobs-dev
Section: libdevel
Architecture: any
Depends:
 libobs0 (= ${binary:Version}),
 libsimde-dev,
 ${misc:Depends}
Description: recorder and streamer for live video content (development files)
 OBS Studio is designed for efficiently recording and streaming live video
 content. It supports live RTP streaming to various streaming sites.
 .
 This package contains the development files.
